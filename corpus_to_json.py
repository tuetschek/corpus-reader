#!/usr/bin/env python3

from benchmark_reader import Benchmark
from benchmark_reader import select_files
import json


DATA_PATH = '../webnlg-dataset/release_v3.0/'
OUT_PATH = '.'


def convert_data(data_path):
    """Returns converted data in a dict (lang, fold) -> instances."""
    data = {}
    for lang in ["en", "ru"]:
        for fold in ["train", "dev", "test"]:
            if fold != "test":
                files = select_files(f'{DATA_PATH}/{lang}/{fold}')
            else:
                # only a single file with all triples
                files = [(f"{DATA_PATH}/{lang}/{fold}", f"rdf-to-text-generation-test-data-with-refs-{lang}.xml")]

            b = Benchmark()

            # note that this only appends data without any clearing
            b.fill_benchmark(files)
            data_sect = []

            for idx, entry in enumerate(b.entries):
                triples = entry.modifiedtripleset.triples

                in_triples = [t.flat_triple() for t in triples]

                out_list = []
                for lex in entry.lexs:
                    # russian data contains english lexicalizations, ignore them
                    if lang=="ru" and lex.lang == "en":
                        continue

                    out_txt = lex.lex
                    out_list.append(out_txt)

                data_sect.append({'input': in_triples,
                                  'target': out_list,
                                  'category': entry.category,
                                  'id': idx,
                                  'webnlg-id': f'{fold}/{entry.category}/{entry.size}/{entry.id}'})

            data[lang, fold] = data_sect

    return data


def write_data(data, out_path):
    """Writes all languages and folds of the data in the GEM JSON format into the given directory."""
    for (lang, fold), data_sect in data.items():
        with open(f'{out_path}/webnlg_{lang}_{fold}.json', 'w', encoding='UTF-8') as fh:
            json.dump({'language': lang, 'values': data_sect}, fh, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    data = convert_data(DATA_PATH)
    write_data(data, OUT_PATH)
